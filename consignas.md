﻿### CASO 1

 Dada las siguientes funciones

  ```javascript
    var Foo = function( a ) {
      var baz = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };

    Foo.prototype = {
      biz: function() {
        return a;
      }
    };

    var f = new Foo( 7 );
    f.bar(); 
    f.baz(); 
    f.biz(); 
  ```

1. ¿Cuál es el resultado de las últimas 3 líneas?
En primer lugar el código traería error dado que f.bar(); no es una  función, no esta declarada  en este código
    Por otro lado no tenemos una salida impresa así que por más que tengan un valor no se van a mostrar, un alert, console.log o usando innerHTML
    En cuanto a la función f.baz(); solo retorna el valor que se le hadadoa la  variable  a (que tampoco esta declarada, habría que declararla) en este caso el valor que retornaría es un 7
    La funcion f.biz(); trae una variable indefinida porque no recibe ningún dato en el prototype.

2. Modificá el código de `f.bar()` para que retorne `7`?

    var a;
    var Foo = function( a ) {
      var baz = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };
	var bar = function() {
        return a;
      };
      this.bar = function() {
        return a;
      };
    };

    Foo.prototype = {
      biz: function() {
        return a;
      }
    };

    var f = new Foo( 7 );
    alert(f.bar()); 
    alert(f.baz()); 
    console.log(f.biz()); 
  ```



 3. Modificá el código para que `f.biz()` también retorne `7`?

    var a;
    var Foo = function( a ) {
      var baz = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };
	var bar = function() {
        return a;
      };
      this.bar = function() {
        return a;
      };
    };
      var biz= function() {
        return a;
      };
      this.biz = function() {
        return a;
      };
    };

    var f = new Foo( 7 );
    alert(f.bar()); 
    alert(f.baz()); 
    console.log(f.biz()); 
  ```


### CASO 2

Partiendo del siguiente array:

```javascript
var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];
```

¿Cómo podrías ordenarlo de la siguiente forma?:

```javascript
[
  { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
  { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
  { skill: 'html', users: [ 'Sue' ], count: 1 }
]
```
Respuesta:
Tendria que convertir el array a Json, con esta sentencia:
var myJSON = JSON.stringify(endorsements);