﻿### ALGORITMOS

Escribir una función simple (no más de 100 caracteres) que retorne un boolean indicando si un string
dado es un palíndromo.

function isPalindrome(string) {
 var x = string.length; 
 var reverse = ""; 
 while (x>=0) { 
  reverse = reverse + string.charAt(x); x--; 
 } 
return (string === reverse); };
var a = isPalindrome('ana'); 
alert(a);


### JAVASCRIPT 

What will the code below output to the console and why?

```javascript
var myObject = {
    foo : "bar",
    func:function() {
        var self=this;
        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);
        (function(){
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);   
        }());
    }
};

myObject.func();
```
La salida es:
outer func: this.foo = bar 
outer func: self.foo = bar 
inner func: this.foo = undefined 
inner func: self.foo = bar

Porque en javascript el this depende del contexto en cual se encuentra, porque es dinámico y se guarda en self  para tener siempre la referencia original del objeto.


a. What is a Promise?

A promise is an object that may produce a single value some time in the future: either a resolved value, or a reason that it’s not resolved (e.g., a network error occurred). A promise may be in one of 3 possible states: fulfilled, rejected, or pending. Promise users can attach callbacks to handle the fulfilled value or the reason for rejection.

b. What is ECMAScript?

JavaScript is a subset of ECMAScript. JavaScript is basically ECMAScript at its core but builds upon it. Languages such as ActionScript, JavaScript, JScript all use ECMAScript as its core

c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?

The NaN (Not-a-Number) is a weirdo Global Object in javascript frequently returned when some mathematical operation failed. You wanted to check if NaN == null which results false . Hovewer even NaN == NaN results with false . A Simple way to find out if variable is NaN is an global function isNaN()

d. What will be the output when the following code is executed? Explain.

```javascript 
console.log(false=='0')
console.log(false==='0')
```
Output:
True
false

Because when comparing a value with two signs, it does not take into account the type of data, when it is with three if and the comparison is exact.












